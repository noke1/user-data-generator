# User data generator

GUI wrapper around random user api.
Api used: https://randomuser.me/

# Usage

1. Clone the project to your local machine
2. Navigate to the root directory
3. Open the terminal and run `npm run dev` - This stepcan be also achieved by opening the project via Visual Studio Code, and opening the terminal inside the IDE.
4. Open the browser and visit the `http://localhost:5556` (the default port is set to `5556`. To change that visit the `server.js` file)
5. Choose `Random` or `Specific` tab to generate the random user data.
