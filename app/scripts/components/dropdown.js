const minLength = document.querySelector("#min-length")
const maxLength = document.querySelector("#max-length")

export const fillPAsswordDropdowsWIthValues = () => {
  const passwordMinLength = 1;
  const passwordMaxLength = 64;
  const defaultPasswordMinLength = 8;
  const defaultPasswordMaxLength = 64;

  for(let i = passwordMinLength; i <= passwordMaxLength; i++){
    let theOption = new Option;
    theOption.text = i;
    theOption.value = i;
    minLength.options[i] = theOption;
  }

  for(let i = passwordMinLength; i <= passwordMaxLength; i++){
    let theOption = new Option;
    theOption.text = i;
    theOption.value = i;
    maxLength.options[i] = theOption;
  }

  const mn = document.querySelector(`#min-length option[value="${defaultPasswordMinLength}"]`)
  mn.setAttribute('selected', true)
  const mx = document.querySelector(`#max-length option[value="${defaultPasswordMaxLength}"]`)
  mx.setAttribute('selected', true)
}