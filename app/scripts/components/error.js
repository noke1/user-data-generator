const pillsTabContent= document.querySelector('#pills-tabContent')

export const displayError = (msg) => {
  let error = document.createElement('div')
  error.setAttribute('role','alert')
  error.classList.add('alert')
  error.classList.add('alert-danger')
  error.classList.add('alert-dismissible')
  error.classList.add('fade')
  error.classList.add('show')
  error.innerHTML += msg

  let closeButton = document.createElement('button')
  closeButton.setAttribute('type','button')
  closeButton.setAttribute('data-bs-dismiss','alert')
  closeButton.setAttribute('aria-label','close')
  closeButton.classList.add('btn-close')

  error.appendChild(closeButton)
  pillsTabContent.appendChild(error)
};