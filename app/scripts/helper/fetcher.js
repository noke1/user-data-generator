import { displayAddressData, displayImage, displayPersonalData } from './formatter.js'
import { displayError } from '../components/error.js'
import { genderSettings, passwordGenerationCharactersSettings, passwordGenerationLengthSettings } from './settings.js'

const apiBaseUrl = 'https://randomuser.me/api/'

export const sendRequestForSpecificUser = () => {
  sendRequest(prepareEndpointWithSettings())
}

export const sendRequestForRandomUser = () => {
  sendRequest(apiBaseUrl)
}

const sendRequest = endpoint => {
  fetch(endpoint)
    .then(res => res.json())
    .then(data => {
      displayPersonalData(data)
      displayAddressData(data)
      displayImage(data)
    }).catch((err) => {
      displayError(`Something went horribly wrong. See: ${err}`)
    })
}

const prepareEndpointWithSettings = () => {
  const gender = genderSettings()
  const passwordCharacters = passwordGenerationCharactersSettings()
  const passwordLength = passwordGenerationLengthSettings()
  return `${apiBaseUrl}?gender=${gender}&password=${passwordCharacters},${passwordLength}`
}

