const titleField = document.querySelector('#title')
const fullnameField = document.querySelector('#fullname')
const genderField= document.querySelector('#gender')
const cityField= document.querySelector('#city')
const streetField= document.querySelector('#street')
const countryField= document.querySelector('#country')
const stateField= document.querySelector('#state')
const userImage= document.querySelector('#user-image img')
const usernameField = document.querySelector('#username')
const passwordField= document.querySelector('#password')

export const displayPersonalData = data => {
  const { 
    gender, 
    name: {
      last, 
      title,
      first
    },
    login: {
      username,
      password
    }
  } = data.results[0];
  
  titleField.value = title;
  fullnameField.value = `${first} ${last}`;
  genderField.value = gender;
  usernameField.value = username;
  passwordField.value = password;
};
  
export const displayAddressData = data => {
const { 
    location: {
      street: {
        number,
        name
      },
      city,
      state,
      country
    }
  } = data.results[0];

  cityField.value = city;
  stateField.value = state;
  countryField.value = country;
  streetField.value = `${number} ${name}`;
};
  
export const displayImage = data => {
  const { 
    picture: {
      large: image
    }
  } = data.results[0];

  userImage.src = image;
};