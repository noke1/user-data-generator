const genderOptions = document.querySelectorAll('#gender-pick input')
const passwordOptions = document.querySelectorAll('#password-pick input')
const minLength = document.querySelector("#min-length")
const maxLength = document.querySelector("#max-length")

export const genderSettings = () => {
  const genders = [...genderOptions]
  const chosenGender = genders.filter(x => x.checked).map(x => x.getAttribute('value'))[0]
  return chosenGender
};
  
export const passwordGenerationCharactersSettings = () => {
  const passwords = [...passwordOptions]
  const chosenPasswords = passwords.filter(x => x.checked).map(x => x.getAttribute('value'))
  return chosenPasswords.join(',')
};

export const passwordGenerationLengthSettings = () => {
  const chosenMinLength = +minLength.value;
  const chosenMaxLength = +maxLength.value;
  
  if(chosenMinLength > chosenMaxLength || chosenMaxLength < chosenMinLength) {
    console.error("not allowed!!")
  }
  return `${chosenMinLength}-${chosenMaxLength}`
};