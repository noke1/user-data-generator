import { sendRequestForSpecificUser } from './helper/fetcher.js'
import { sendRequestForRandomUser } from './helper/fetcher.js'
import { fillPAsswordDropdowsWIthValues } from './components/dropdown.js'

const getSpecificUserData = document.querySelector('#generate-specific')
const getRandomUserData = document.querySelector('#generate-random')

if(window.location.href.includes('specific')) {
    getSpecificUserData.addEventListener('click', () => sendRequestForSpecificUser())
    window.addEventListener('load', () => fillPAsswordDropdowsWIthValues())
}

if(window.location.href.includes('random')) {
    getRandomUserData.addEventListener('click', () => sendRequestForRandomUser())
}