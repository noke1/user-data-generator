const express = require('express')
const app = express()
const port = 5556

app.use(express.static('app'))
app.listen(port)